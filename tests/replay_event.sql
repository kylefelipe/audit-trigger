begin;

select plan(8);

-- setup a table in search_path
create table test_replay(
  id serial primary key,
  description text
);
-- data
insert into test_replay(description) values ('version 1');
select is((select description from test_replay), 'version 1');
-- setup audit
select audit.audit_table('test_replay');
-- alter the value
update test_replay set description = 'v2';
-- remember this event_id
select max(event_id) as event_id from audit.logged_actions \gset
select is((select description from test_replay), 'v2');
update test_replay set description = 'v3';
select is((select description from test_replay), 'v3');
-- replay the first event
select audit.replay_event(:event_id);
select is((select description from test_replay), 'v2', 'test_replay should revert to previous state');

-- setup a table in another schema

create schema foo;
-- setup a table in search_path
create table foo.test_replay(
  id serial primary key,
  description text
);
-- setup audit
select audit.audit_table('foo.test_replay');
-- data
insert into foo.test_replay(description) values ('foo version 1');
select is((select description from foo.test_replay), 'foo version 1');
-- alter the value
update foo.test_replay set description = 'foov2';
-- remember this event_id
select max(event_id) as event_id from audit.logged_actions \gset
select is((select description from foo.test_replay), 'foov2');
update foo.test_replay set description = 'foov3';
select is((select description from foo.test_replay), 'foov3');
-- replay first event
select audit.replay_event(:event_id);
select is((select description from foo.test_replay), 'foov2');

-- with a view
-- FIXME I think commit f5e235f completely broke views, because instead of triggers break the editable views
-- uncomment after fixing this
/*
create view foo.test_view as select id, description from foo.test_replay;
select is((select description from foo.test_view), 'foo version 1');
select audit.audit_view('foo.test_view', ARRAY['id']);
-- alter the value
update foo.test_view set description = 'v3';
select is((select description from foo.test_view), 'v3');
-- replay
select audit.replay_event((select max(event_id) from audit.logged_actions));
select is((select description from foo.test_view), 'foo version 1');
*/

rollback;
